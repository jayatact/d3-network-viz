
var margin = {
    top : 40,
    right : 80,
    bottom : 80,
    left : 40
}
var svg = d3.select("body").append("svg")
    .attr("width", window.innerWidth-margin.right-margin.left);
    .attr("height", window.innerHeight-margin.top-margin.bottom);

var force = d3.layout.force()
    .gravity(.00)
    .distance(0)
    .charge(0)
    .size([width, height]);


var graphFile = getData();
d3.json(graphFile, function(json) {
  console.log(json);
  force
      .nodes(json.nodes)
      .links(json.links)
      .start();

  var link = svg.selectAll(".link")
      .data(json.links)
    .enter().append("line")
      .attr("class", "link")
    .style("stroke-width", function(d) { return 1; })
    .style("stroke-color", function(d){return "#FF00FF";});

  var node = svg.selectAll(".node")
      .data(json.nodes)
    .enter().append("g")
      .attr("class", "node")
      .call(force.drag);

  node.append("circle")
      .attr("r","5");

  node.append("text")
      .attr("dx", 12)
      .attr("dy", ".35em")
      .text(function(d) { return d.name });

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
  });
});