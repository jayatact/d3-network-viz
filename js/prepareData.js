var getData = (function(){
	
	return {

		get_edge_list: function(nodes){

			edge_list=[];
						master_pair_list.forEach(function(i){
							p1 = indexinhash(nodes,i["source"], "person");
							p2 = indexinhash(nodes,i["target"], "person");
							if(typeof p1 != "undefined" && typeof p2!= "undefined"){
								edge_list.push({"source": p1,"target": p2});
							}
						});
						//console.log(edge_list);
			return edge_list;

			function indexinhash(hasharray, value, key){

				var i=0;
				while(i<hasharray.length){
					if(hasharray[i][key]==value){
						return i;
					}
					i++;
				}
			}
		},

		get_node_list: function(criteria, value){
			var process_data = function(){
				var uniqueArray = master_list.reduce(function (a, d) {
				       if (a.indexOf(d.person) === -1) {
				         a.push(d);
				       }
				       return a;
				    }, []);
				return uniqueArray;
			};

			pair_list = process_data();
			//console.log(pair_list.slice(-100));
			if(criteria == 'undefined') {return pair_list;}
			else{
				
				temp_list = pair_list.filter(function(v){
					//console.log(v, criteria, value);
					return v[criteria]==value;})
				console.log(temp_list.length);
				return temp_list;
			}
		},

		cluster_node_list: function(nodes, criteria, clusters){
		var maxRadius = 10;
		var noOfClusters = nodes.filter(function(v,i) { return i==nodes.lastIndexOf(v[criteria]); }).length; 
		var clusters = new Array(noOfClusters);
		var clusterNodes = nodes.map(function(el){
			var i= Math.floor(Math.random() * noOfClusters),
			r = Math.sqrt((i + 1) / noOfClusters * -Math.log(Math.random())) * maxRadius;

			el["cluster"]= i;
			el["radius"]= r;
  			if (!clusters[i] || (r > clusters[i].radius)) clusters[i] = el;
  			return el;
		}); 
		return [clusterNodes, clusters];
		}, 
	

	    add_attr: function(attr_name, value){ //Functional way of adding an attribute to the hash
		return function(item){
			item[attr_name]= value;
			return item;
		}
	}
	}

	var auto_update_num =  function(attr_name, amount){ //Use autonumber for numeric field by fixed amount
		return function(item){
			 
			item[attr_name] = amount + item[attr_name];
			return current
		}
	};

	var populate_edges = function(criteria){// create edges based on a criteria using REDUCE ONLY
		return function(item){
			if(item[criteria]===current[criteria]){
					return {"source": prev[criteria], "target": current[criteria]}
			}
		}
	};




}());