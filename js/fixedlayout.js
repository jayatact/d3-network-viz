var drawViz= function(filters){
	console.log("Viz was drawn", filters);
	var margin = {
    top : 40,
    right : 80,
    bottom : 80,
    left : 40
} 
  var w = window.innerWidth-margin.right-margin.left,
      h = window.innerHeight-margin.top-margin.bottom;
	var svg = d3.select("#svgContent")
                  .append("svg")
                  .attr("width", w)
                  .attr("height", h)
                  //.attr('preserveAspectRatio', 'xMinYMin slice') 
                  .append('g');


     ///Only to fill blank area
     svg.append("rect")
		  .attr('fill','none')
		  .attr('pointer-events', 'all')
		  .attr("width", w + margin.left + margin.right)
		  .attr("height", h + margin.top + margin.bottom)
		  .attr("class","chartArea");


     var radius= 6;
    //Data Prep
	var nodes = getData.get_node_list("state", filters["state"]);

	var edges= getData.get_edge_list(nodes);

	/*var clusterNodes = getData.cluster_node_list(nodes, "city");
	nodes = clusterNodes[0];
	var edges= getData.edge_list(nodes, "Pair#");
	clusters = clusterNodes[1];*/
	//dataSet.nodes.map(function(item){item["fixed"]=true;});
	//dataSet.nodes.filter(function(value){value["state"]="AL"}); 
	/* {
	
			nodes: [
              { name: "Sara", id:"1", x:239.31191418045182, y:296.78520431471384, fixed:true},
              { name: "Raul", id:"2", x:261.1651006560272, y:200.78448049334696, fixed:true},
              { name: "Stefano", id:"3", fixed:false},
              { name: "Michele", id:"4", fixed:false}
			],
			dataSet.edges= [
              { source: 0, target: 1 },
              { source: 1, target: 0 },
              { source: 1, target: 2 },
              { source: 2, target: 1 },
              { source: 2, target: 3 }
			];*/
		
		//Viz code	
		var c20 = colorscale(nodes, "city");
		
		var force = d3.layout.force()
			.nodes(nodes)
			.links(edges)
			.gravity(0.5)
            .distance(10)
            .charge(-120)
			.size([w,h])
			.start();

		var link = svg.selectAll(".link")
			.data(edges)
			.enter().append("line")
			.attr("class", "link")
			.on("mouseover", function(d){nodeTooltip(d,["id"], "link")});

			/*.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });*/

		var node_drag = d3.behavior.drag()
			.on("dragstart", dragstart)
			.on("drag", dragmove)
			.on("dragend", dragend);



        var node = svg.selectAll("circle")
			.data(nodes)
			.enter().append("circle")
			.attr("class", "node")
			.attr("r", 4.5)
			//.attr("cx", function(d) { return d.x; })
			//.attr("cy", function(d) { return d.y; })
			//.style("fill", function(d){return c20(d["hsCodeAttending"]);})
			
			.on("mouseover", function(d){nodeTooltip(d,["person"])})
			.on("mouseout", function(d) {       
		            div.transition()        
		                .duration(500)      
		                .style("opacity", 0);   
		        })
			.on("click", nodeClick)
			.call(node_drag);

		var selected;
		function nodeClick(){       
		   if(!selected){
		   	console.log("NOT SEL", selected);
		     selected = this;
		     d3.select(selected).attr('class', 'node-selected');
		  } 
		  else {
		  		console.log("SEL", selected);
		     d3.select(selected).style('class', 'node');
		     selected = this;
		     d3.select(selected).style('class', 'node-selected');
		  }
		}
		var nodeTooltip = function(d, params, toolType) {      
		            div.transition()        
		                .duration(200)      
		                .style("opacity", .9);      
		            div .html( function(){
		            	if(toolType === "link"){d=d["source"];}
		            	var htmlOutput = "";
		            	
		            	for(var i=0;i<params.length;i++){
		            		htmlOutput = htmlOutput + d[params[i]] + "<br/>" ;
		            	}
		            	return htmlOutput;

		            })  
		                .style("left", (d3.event.pageX) + "px")     
		                .style("top", (d3.event.pageY - 28) + "px");    
		                              
		        };

  		fisheyeView();
  		function fisheyeView(){
		var fisheye = d3.fisheye.circular()
		.radius(120);
		svg.on("mousemove", function() {
		      force.stop();
		      fisheye.focus(d3.mouse(this));
		      d3.selectAll("circle").each(function(d) { d.fisheye = fisheye(d); })
		          .attr("cx", function(d) { return d.fisheye.x; })
		          .attr("cy", function(d) { return d.fisheye.y; })
		          .attr("r", function(d) { return d.fisheye.z * 4.5; });
		      link.attr("x1", function(d) { return d.source.fisheye.x; })
		          .attr("y1", function(d) { return d.source.fisheye.y; })
		          .attr("x2", function(d) { return d.target.fisheye.x; })
		          .attr("y2", function(d) { return d.target.fisheye.y; });
		    });
  		}

		function dragstart(d, i) {
			force.stop(); // stops the force auto positioning before you start dragging
		}

		function dragmove(d, i) {
			d.px += d3.event.dx;
			d.py += d3.event.dy;
			d.x += d3.event.dx;
			d.y += d3.event.dy; 
			tick(); 
		}

		function dragend(d, i) {
			d.fixed = true; // of course set the node to fixed so the force doesn't include the node in its auto positioning stuff
			tick();
		}

		function colorscale(nodes, param){
			console.log(nodes);
			return  d3.scale.category20().
					domain(nodes.map(function(d){return d[param]; }));
		
		}
		force.on("tick", tick);

		//This snippet of code prevents initial floating of force layout
		var n=10;
      	for (var i = n * n; i > 0; --i) {force.tick();}
		  force.stop();


		function tick() {
          link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          .attr("x2", function(d) { return d.target.x; })
          .attr("y2", function(d) { return d.target.y; });

		node.attr("cx", function(d) { return d.x = Math.max(radius, Math.min(w - radius, d.x)); })
        .attr("cy", function(d) { return d.y = Math.max(radius, Math.min(h - radius, d.y)); });
				}




		var div = d3.select("body").append("div")   
    		.attr("class", "tooltip")               
    		.style("opacity", 0);


};


window.onload = function(){
	var stateVal = "";
	//$("#selectState>select").change(function(stateVal = $("#selectState>select").val();){});
	$('#selectState > button').click(function(event){
		stateVal = $('#selectState > select').val();

		filters={"state": stateVal};
		$('#svgContent').empty();
		nodelist = getData.get_node_list("state", filters["state"]);
		edgelist =getData.get_edge_list(nodelist);
		//console.log(nodelist);
		//console.log(edgelist);
		drawViz(filters);
	});

	
	
}